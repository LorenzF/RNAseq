# RNAseq Analysis

A simple bash oriented method of obtaining FastQ files and analysing them with tophat and bowtie, as of today this analysis might be outdated and should be used only for educational purposes.

## Getting Started

Clone the git repository in your supercluster, the code is designed to operate by adding tophat alignment jobs to the queue.
List.txt contains csv formatted entries of samples from the SRA archive containing experiment, sample and run.
For each entry the code will recursively check if the required data is available to calculate the gene counts with featurecount

### Prerequisites

The repository needs several packages: 
- fastqc
- tophat
- bowtie2
- samtools
- featureCount

<!---
```
Give examples
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 
--->
## Authors

* **Lorenz Feyen** - *Initial work* - [LorenzF](https://gitlab.com/LorenzF)

<!---See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.--->

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* None

