#!/bin/bash

##Parameters##
maxpairs=20000000
#module load SAMtools/0.1.19-intel-2014a
#module load HTSeq/0.6.1p1-intel-2014a-Python-2.7.6

echo "########################################################"
echo "##########Automatic sample processor started############"
echo "########################################################"

while read line
do

#echo $line;

input=( ${line//,/ } )
experiment=${input[0]}
sample=${input[1]}
run=${input[2]}

if [ ! -e "$VSC_DATA/VSC/featureCount/${run}.count" ]; then
  if [ ! -e "$VSC_SCRATCH/${experiment}/${run}/accepted_hits.bam" ]; then
    if [ ! -e "$VSC_SCRATCH/${experiment}/${run}/${run}.fastq" ] && { [ ! -e "$VSC_SCRATCH/${experiment}/${run}/${run}_1.fastq" ] || [ ! -e "$VSC_SCRATCH/${experiment}/${run}/${run}_2.fastq" ]; }; then
      if [ ! -d "$VSC_SCRATCH/${experiment}/${run}" ]; then
        if [ ! -d "$VSC_SCRATCH/${experiment}" ]; then
          mkdir $VSC_SCRATCH/${experiment}
        fi
        mkdir $VSC_SCRATCH/${experiment}/${run}
      fi

      while [ $(qstat -u vsc31526 | wc -l) -gt 0 ]; do
        sleep 60
      done

      $VSC_DATA/SRAtoolkit_2.8.1/bin/fastq-dump.2.8.1 --split-3 -X ${maxpairs} -O $VSC_SCRATCH/${experiment}/${run} ${run}
      #$VSC_DATA/SRAtoolkit_2.8.1/bin/fastq-dump.2.8.1 --split-3 -O $VSC_SCRATCH/${experiment}/${run} ${run}
      rm -r $VSC_HOME/ncbi
    fi

    while [ $(qstat -u vsc31526 | wc -l) -gt 0 ]; do
      sleep 60
    done

  fi

  echo "Processing sample '${run}' in '${sample}' from experiment '${experiment}'"
  qsub -N ${run} -v experiment=${experiment},sample=${sample},run=${run} $VSC_HOME/job.pbs

  sleep 10

  while [ $(qstat -u vsc31526 | wc -l) -gt 0 ]; do
    sleep 60
  done

fi


done < "./list.txt"

